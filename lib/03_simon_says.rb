def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, num = 2)
  [word] * num * ' '
end

def start_of_word(word, num)
  splits = word.split('')
  splits[0..(num - 1)].join('')
end

def first_word(sent)
  # splits = sent.split(' ')
  # splits[0]
  sent.split(' ').select.with_index {|word, index| index == 0 }.join("")
end
# Why doesn't this work???
def titleize(sent)
  splits = sent.split(' ')
  results = []
  splits.each_index do |idx|
    if idx.zero?
      results << splits[idx].capitalize
    elsif splits[idx] == 'and' || splits[idx] == 'over' ||splits[idx] == 'the'
      results << splits[idx]
    else
      results << splits[idx].capitalize
    end
  end
  results.join(' ')
end
