def translate(sentence) #hello => ellohay
splits = sentence.split(" ")
splits.map! do |word|
  latinize(word)
end
splits.join(" ")
end

def latinize(word)
  vowels = 'aeioqu'

  until vowels.include?(word[0].downcase)
    if word[0..1] == 'qu' #&& word[1] == 'u'
      word = word[2..-1] + word[0..1]
    else
       word = word[1..-1] + word[0]
    end

  end
  word + "ay"
end
