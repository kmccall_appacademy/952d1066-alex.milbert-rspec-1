def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  ans = arr.reduce(:+)
  if arr.empty?
    return 0
  else
    return ans
  end

end

def multiply(numbers)
  numbers.reduce(:*)
end

def power(num1, num2)
  num1**num2
end

def factorial(number)
    number == 0 ? 0 : (1..number).select {|n| number % n == 0}
end
